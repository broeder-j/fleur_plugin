Plug-in Classes
===============


Fleur input generator plug-in
+++++++++++++++++++++++++++++


Fleurinputgen Calculation
-------------------------
.. automodule:: plugin_files.aiida.orm.calculation.job.fleur_inp.fleurinputgen.fleurinputgenCalculation
   :members:

Fleurinputgen Parser
--------------------
.. automodule:: plugin_files.aiida.parsers.plugins.fleur_inp.fleur_inputgen.fleur_inputgenParser
   :members:


Fleur input Data structure
++++++++++++++++++++++++++


FleurinpData
------------
.. automodule:: plugin_files.aiida.orm.data.fleurinp
   :members:
   :special-members: __init__

Fleur plug-in
+++++++++++++

Fleur Calculation
-----------------
.. automodule:: plugin_files.aiida.orm.calculation.job.fleur_inp.fleur
   :members:

Fleur Parser
------------
.. automodule:: plugin_files.aiida.parsers.plugins.fleur_inp.fleur
   :members:

