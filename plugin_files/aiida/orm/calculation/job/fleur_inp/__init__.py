# -*- coding: utf-8 -*-
'''
init file for the input FLEUR input plug-ins. What are common features?
'''

__copyright__ = u"Copyright (c), 2016, Forschungszentrum Juelich, Germany. All rights reserved."
__license__ = ""
__version__ = "0.27"
__contributors__ = "Jens Broeder"
