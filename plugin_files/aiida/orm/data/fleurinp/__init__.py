# -*- coding: utf-8 -*-
"""
In this module is the FleurinpData class, and useful methods for FLEUR input
manipulation.
"""

# TODO: source out and import some xml stuff, which will also be needed by the
# parser (also inputgen Parser) later
# TODO: inpxml to dict: kpts should not be writen to the dict? same with symmetry
# TODO: All methods are accessible by the outside! no private. try to reduce
# number of class methods
# TODO: test for large input files, I believe the recursion is still quite slow..
# TODO: rewrite .copy
# TODO: 2D cell get kpoints and get structure

__copyright__ = (u"Copyright (c), 2016, Forschungszentrum Jülich GmbH, "
                 "IAS-1/PGI-1, Germany. All rights reserved.")
__license__ = "MIT license, see LICENSE.txt file"
__version__ = "0.27"
__contributors__ = "Jens Broeder"

import os
import re
from lxml import etree
from lxml.etree import XMLSyntaxError
from aiida import load_dbenv, is_dbenv_loaded
if not is_dbenv_loaded():
    load_dbenv()

from aiida.orm import Data
from aiida.common.exceptions import InputValidationError, ValidationError
from aiida.common.exceptions import UniquenessError
from aiida.common.utils import get_repository_folder

#from aiida.orm.data.folder import FolderData
#from aiida.parsers.plugins.fleur_inp.fleur_inputgen import inpxml_todict

class FleurinpData(Data):
    """
    AiiDA data object representing everything a FLEUR calculation needs.

    It is initialized with an absolute path to an inp.xml file.
    Other files can also be added, but are not represented and used so far. (TODO ?)

    It will store the files in the repository and store the input parameters of the
    inp.xml file of FLEUR in the Database as a python dictionary (as internal attributes).
    When an inp.xml (name important!) file is added to files, FleurinpData searches
    for a corresponding xml schema file in the PYTHONPATH environment variable.
    Therefore, it is recommened to place all schema files in the plug-in source code directory.
    If no corresponding schema file is found an error is raised.

    FleurinpData further provides the user with manipulation methods for the inp.xml file and
    methods to extract AiiDA StructureData and KpointsData nodes.

    It tracks the changes made by the user on the inp.xml file in a dictionary.
    The plug-in needs to know, what files change, because files should only be uploaded
    if changed, otherwise copied remotely. (for now the logic is simple: always copy)

    Remember that most attributes of AiiDA nodes can not be changed after they
    have been stored in the DB! Therefore, you often have to copy first a
    FleurinpData object, if you want to change somthing in the inp.xml file and
    start a new calculation from it.
    """


    #_has_schema = False
    #_schema_file_path = None
    # serach in current folder and search in aiida source code
    # we want to search in the Aiida source directory, get it from python path,
    # maybe better from somewhere else.
    #TODO: dont walk the whole python path, test if dir below is aiida
    pythonpath = os.environ['PYTHONPATH'].split(':')
    _search_paths = ['./']
    for path in pythonpath[:-1]:
        _search_paths.append(path)
    #_search_paths = ['./', '/Users/broeder/aiida/codes/fleur/',
    #                 str(get_repository_folder())]


    @property
    def _has_schema(self):
        """
        Boolean property, which stores if a schema file is already known
        """
        return self.get_attr('_has_schema', False)
    @property
    def _schema_file_path(self):
        """
        A string, which stores the absolute path to the schemafile fount
        """
        return self.get_attr('_schema_file_path', None)

    @_has_schema.setter
    def _has_schema(self, boolean):
        """
        Setter for has_schema
        """
        self._set_attr('_has_schema', boolean)

    @_schema_file_path.setter
    def _schema_file_path(self, schemapath):
        """
        Setter for the schema file path
        """
        self._set_attr('_schema_file_path', schemapath)

    # files
    @property
    def files(self):
        """
        Returns the list of the names of the files stored
        """
        return self.get_attr('files', [])

    @files.setter
    def files(self, filelist):
        """
        Add a list of files to FleurinpData.
        Alternative use setter method.

        :param files: list of filepaths
        """
        for file1 in filelist:
            self.set_file(file1)

    def set_files(self, files):
        """
        Add a list of files to FleurinpData
        Alternative setter

        :param files: list of filepaths
        """
        self.files = files

    def set_file(self, filename):
        """
        Add a file to the FleurinpData

        :param filename: absolute path to the file
        """
        self._add_path(filename)

    def del_file(self, filename):
        """
        Remove a file from FleurinpData

        :param filename: name of the file stored in the DB
        """
        # remove from files attr list
        if filename in self.get_attr('files'):
            try:
                self.get_attr('files').remove(filename)
                #self._del_attr('filename')
            except AttributeError:
                ## There was no file set
                pass
        # remove from sandbox folder
        if filename in self.get_folder_list():
            super(FleurinpData, self).remove_path(filename)

    def get_file_abs_path(self, filename):
        """
        Return the absolute path to a file in the repository

        :param filename: name of the file

        """
        if filename in self.files:
            return os.path.join(self._get_folder_pathsubfolder.abspath, filename)
        else:
            raise ValueError(
                '{} is not in {}'.format(filename,
                    os.path.join(self._get_folder_pathsubfolder.abspath)))

    def find_schema(self, inp_version_number):
        """
        Method which searches for a schema files (.xsd) which corresponds
        to the input xml file. (compares the version numbers)
        """
        # user changed version number, or no file yet known.
        # TODO test if this still does the right thing if user adds one
        #inp.xml and then an inp.xml with different version.
        #print 'here'
        schemafile_paths = []
        for path in self._search_paths:#paths:
            for root, dirs, files in os.walk(path):
                for file1 in files:
                    if file1.endswith(".xsd"):
                        if ('Fleur' in file1) or ('fleur' in file1):
                            schemafile_path = os.path.join(root, file1)
                            print schemafile_path
                            schemafile_paths.append(schemafile_path)
                            i = 0
                            imin = 0
                            imax = 0
                            schemafile = open(schemafile_path, 'r')
                            for line in schemafile.readlines():
                                i = i + 1
                                if re.search('name="FleurVersionType"', line):
                                    imax = i + 3
                                    imin = i
                                if (i > imin) and (i <= imax):
                                    if re.search('enumeration value', line):
                                        schema_version_number = re.findall(r'\d+.\d+', line)[0]
                                        #print 'schemaversion number: ' + str(schema_version_number)
                                        break
                            schemafile.close()
                            if schema_version_number == inp_version_number:
                                #we found the right schemafile for the current inp.xml
                                #print 'hier', schemafile_path
                                self._set_attr('_schema_file_path', schemafile_path)
                                self._set_attr('_has_schema', True)
                                #self.set_schema_file_path = schemafile_path
                                #self.set_has_schema = True
                                #print self._schema_file_path
                                #print self._has_schema
                                return schemafile_paths
        return schemafile_paths

    def _add_path(self, src_abs, dst_filename=None):
        """
        Add a single file to folder

        """
        #TODO, only certain files should be allowed to be added
        #_list_of_allowed_files = ['inp.xml', 'enpara', 'cdn1', 'sym.out', 'kpts']

        old_file_list = self.get_folder_list()

        if not os.path.isabs(src_abs):
            raise ValueError("Pass an absolute path for src_abs")

        if not os.path.isfile(src_abs):
            raise ValueError("src_abs must exist and must be a single file")

        if dst_filename is None:
            final_filename = os.path.split(src_abs)[1]
        else:
            final_filename = dst_filename

        super(FleurinpData, self).add_path(src_abs, final_filename)

        old_files_list = self.get_attr('files', [])

        if final_filename not in old_file_list:
            old_files_list.append(final_filename)
        self._set_attr('files', old_files_list)

        # here this is hardcoded, might want to change? get filename from elsewhere
        if final_filename == 'inp.xml':
            #get input file version number
            inpfile = open(src_abs, 'r')
            for line in inpfile.readlines():
                if re.search('fleurInputVersion', line):
                    inp_version_number = re.findall(r'\d+.\d+', line)[0]
                    print 'inpversion number: ' + str(inp_version_number)
                    break
            inpfile.close()
            #schemafile_paths = []
            # search for Schema file with same version number
            schemafile_paths = self.find_schema(inp_version_number)
            '''
            def find_schema():
                """
                Method which searches for a schema files (.xsd) which corresponds
                to the input xml file. (compares the version numbers)
                """
                # user changed version number, or no file yet known.
                # TODO test if this still does the right thing if user adds one
                #inp.xml and then an inp.xml with different version.
                #print 'here'

                for path in self._search_paths:#paths:
                    for root, dirs, files in os.walk(path):
                        for file in files:
                            if file.endswith(".xsd"):
                                if ('Fleur' in file) or ('fleur' in file):
                                    schemafile_path = os.path.join(root, file)
                                    print schemafile_path
                                    schemafile_paths.append(schemafile_path)
                                    i = 0
                                    imin = 0
                                    imax = 0
                                    schemafile = open(schemafile_path, 'r')
                                    for line in schemafile.readlines():
                                        i = i + 1
                                        if re.search('name="FleurVersionType"', line):
                                            imax = i + 3
                                            imin = i
                                        if (i>imin) and (i <= imax):
                                            if re.search('enumeration value', line):
                                                schema_version_number = re.findall(r'\d+.\d+', line)[0]
                                                #print 'schemaversion number: ' + str(schema_version_number)
                                                break
                                    schemafile.close()
                                    if schema_version_number == inp_version_number:
                                        #we found the right schemafile for the current inp.xml
                                        #print 'hier', schemafile_path
                                        self._set_attr('_schema_file_path', schemafile_path)
                                        self._set_attr('_has_schema', True)
                                        #self.set_schema_file_path = schemafile_path
                                        #self.set_has_schema = True
                                        #print self._schema_file_path
                                        #print self._has_schema
                                        return


                # get schema file version number
                for schemafile_path in schemafile_paths:
                    i =0
                    imin = 0
                    imax = 0
                    schemafile = open(schemafile_path, 'r')
                    for line in schemafile.readlines():
                        i = i + 1
                        if re.search('name="FleurVersionType"', line):
                            imax = i + 3
                            imin = i
                        if (i>imin) and (i <= imax):
                            if re.search('enumeration value', line):
                                schema_version_number = re.findall(r'\d+.\d+', line)[0]
                                #print 'schemaversion number: ' + str(schema_version_number)
                                break
                    schemafile.close()
                    if schema_version_number == inp_version_number:
                        #we found the right schemafile for the current inp.xml
                        self._schema_file_path = schemafile_path
                        self._has_schema = True
                        break
            '''
            if (self._schema_file_path is None) or (not self.inp_userchanges.has_key('fleurInputVersion')):
                schemafile_paths = self.find_schema(inp_version_number)
            if (not self._has_schema) and (self._schema_file_path is None):
                raise InputValidationError("No XML schema file (.xsd) with matching version number {} "
                    "to the inp.xml file was found. I have looked here: {} "
                    "and have found only these schema files for Fleur: {}. "
                    "I need this file to validate your input and to know the structure "
                    "of the current inp.xml file, sorry.".format(inp_version_number,
                                                          self._search_paths, schemafile_paths))

            # set inp dict of Fleurinpdata
            self._set_inp_dict()

    def _set_inp_dict(self):
        """
        Sets the inputxml_dict from the inp.xml file attached to FleurinpData

        1. get inp.xml strucutre
        2. load inp.xml file
        3. call inpxml_to_dict
        4. set inputxml_dict
        """

        # get inpxml structure
        inpxmlstructure = get_inpxml_file_structure()

        # read xmlinp file into an etree with autocomplition from schema
        inpxmlfile = self.get_file_abs_path('inp.xml')

        xmlschema_doc = etree.parse(self._schema_file_path)
        xmlschema = etree.XMLSchema(xmlschema_doc)
        parser = etree.XMLParser(schema=xmlschema, attribute_defaults=True)
        #dtd_validation=True

        tree = etree.parse(inpxmlfile, parser)
        root = tree.getroot()
        '''
        val_res = xmlschema.validate(tree)
        if val_res :
            print ("YEAH!, xml file has validated from tree")
        else:
            print ("Oh NO!, xml file does not validate from tree")

        log = xmlschema.error_log
        error = log.last_error
        '''
        # convert etree into python dictionary
        inpxml_dict = inpxml_todict(root, inpxmlstructure)

        # set inpxml_dict attribute
        self._set_attr('inp_dict', inpxml_dict)


    # tracing user changes
    @property
    def inp_userchanges(self):
        """
        Return the changes done by the user on the inp.xml file.
        """
        return self.get_attr('inp_userchanges', {})

    # dict with inp paramters parsed from inp.xml
    @property
    def inp_dict(self):
        """
        Returns the inp_dict (the representation of the inp.xml file) as it will
        or is stored in the database.
        """
        return self.get_attr('inp_dict', {})

    def set_inpchanges(self, change_dict):
        """
        Does changes directly on the inp.xml file. Afterwards
        updates the inp.xml file representation and the current inp_userchanges
        dictionary with the keys provided in the 'change_dict' dictionary.

        :param change_dict: a python dictionary with the keys to substitute.
                            It works like dict.update(), adding new keys and
                            overwriting existing keys.
        """
        #TODO make some checks
        if self.inp_userchanges is None:
            self._set_attr('inp_userchanges', {})

        # store change dict, to trac changes
        currentchangedict = self.inp_userchanges
        currentchangedict.update(change_dict)
        self._set_attr('inp_userchanges', currentchangedict)

        # load file, if it does not exsist error will be thrown in routine
        inpxmlfile = self.get_file_abs_path('inp.xml')

        if self._has_schema:
           #schema file for validation will be loaded later
           pass
        elif self._schema_file_path != None:
            print ('Warning: The User set the XMLSchema file path manually, your'
                  'inp.xml will be evaluated! If it fails it is your own fault!')
        else:
            print ('Warning: No XMLSchema file was provided, your inp.xml file '
                  'will not be evaluated and parsed! (I should never get here)')

        #read in tree
        tree = etree.parse(inpxmlfile)
        #root = tree.getroot()

        #apply changes to etree
        new_tree = self._write_new_fleur_xmlinp_file(tree, change_dict)

        # somehow directly writing inp.xml does not work, create new one
        inpxmlfile = os.path.join(
                         self._get_folder_pathsubfolder.abspath, 'temp_inp.xml')

        # write new inp.xml, schema evaluation will be done when the file gets added
        new_tree.write(inpxmlfile)
        print 'wrote tree to' + str(inpxmlfile)

        # delete old inp.xml file, not needed anymore
        #TODO maybe do some checks before
        self.del_file('inp.xml')

        # now the new inp.xml file is added and with that the inpxml_dict will
        # be overwritten, and the file validated
        self._add_path(str(inpxmlfile), 'inp.xml')

        # remove temporary inp.xml file
        os.remove(inpxmlfile)


    '''
    def copy(self):
        """
        Method to copy a FleurinpData and keep the proverance.
        (because currently that is not default)
        """
        super(FleurinpData, self).copy()
    '''
    # TODO better validation? other files, if has a schema
    def _validate(self):
        """
        validation method. Check here for properties that have to be there, for
        a valid fleurinpData object.
        """
        #from aiida.common.exceptions import ValidationError

        super(FleurinpData, self)._validate()

        if 'inp.xml' in self.files:
            has_inpxml = True # does nothing so far
        else:
            raise ValidationError('inp.xml file not in attribute "files". '
                                  'FleurinpData needs to have and inp.xml file!')


    '''
        try:
            has_inpxml = 'inp.xml' in self.files
        except AttributeError:
            raise ValidationError("attribute 'filename' not set.")

        if self.files != self.get_folder_list():
            raise ValidationError("The list of files in the folder does not "
                                  "match the 'files' attribute. "
                                  "_files='{}', content: {}".format(
                self.files, self.get_folder_list()))
    '''


    def _write_new_fleur_xmlinp_file(self, inp_file_xmltree, fleur_change_dic):
        """
        This modifies the xml-inp file. Makes all the changes wanted by
        the user or sets some default values for certain modes

        :param inp_file_lines_o xml-tree of the xml-inp file
        :param fleur_change_dic dictionary {attrib_name : value} with all the
               wanted changes.

        return an etree of the xml-inp file with changes.
        """

        xmltree_new = inp_file_xmltree

        #get all attributes of a inpxml file
        xmlinpstructure = get_inpxml_file_structure()

        pos_switch_once = xmlinpstructure[0]
        pos_switch_several = xmlinpstructure[1]
        pos_attrib_once = xmlinpstructure[2]
        #pos_int_attributes_once = xmlinpstructure[3]
        #pos_float_attributes_once = xmlinpstructure[4]
        #pos_string_attributes_once = xmlinpstructure[5]
        pos_attrib_several = xmlinpstructure[6]
        pos_int_attributes_several = xmlinpstructure[7]
        #pos_float_attributes_several = xmlinpstructure[8]
        #pos_string_attributes_several = xmlinpstructure[9]
        #pos_tags_several = xmlinpstructure[10]
        pos_text = xmlinpstructure[11]
        pos_xpaths = xmlinpstructure[12]
        expertkey = xmlinpstructure[13]


        for key in fleur_change_dic:
            if key in pos_switch_once:
                # call routine set (key,value) in tree.
                # TODO: a test here if path is plausible and if exist
                # ggf. create tags and key.value is 'T' or 'F' if not convert,
                # if garbage, exception
                # convert user input into 'fleurbool'
                fleur_bool = convert_to_fortran_bool(fleur_change_dic[key])

                xpath_set = pos_xpaths[key]
                #TODO: check if something in setup is inconsitent?

                # apply change to tree
                #print xmltree_new, xpath_set, key, fleur_bool
                self._xml_set_first_attribv(xmltree_new, xpath_set, key, fleur_bool)

            elif key in pos_attrib_once:
                # TODO: same here, check existance and plausiblility of xpath
                xpath_set = pos_xpaths[key]
                #print xmltree_new, xpath_set, key, fleur_change_dic[key]
                self._xml_set_first_attribv(xmltree_new, xpath_set, key, fleur_change_dic[key])
            elif key in pos_attrib_several:
                # TODO What attribute shall be set? all, one or several specific onces?
                pass
            elif key in pos_switch_several:
                # TODO
                pass
            elif key in pos_text:
                # can be several times, therefore check
                xpath_set = pos_xpaths[key]
                self._xml_set_text(xmltree_new, xpath_set, fleur_change_dic[key])
            elif key == expertkey:
                # posibility for experts to set something, not suported by the plug-in directly
                pass
            else:
                # this key is not know to plug-in
                raise ValidationError(
                    "You try to set the key:'{}' to : '{}', but the key is unknown"
                    " to the fleur plug-in".format(key, fleur_change_dic[key]))
        return xmltree_new
    '''
    def _xml_test_xpath():
        """
        test if an xpath is know to the plugin and test if the path exit in the file.
        if it does not exist in the file but is valid, the tags should be created.
        """
        pass

    def _xml_create_tag():
        """
        create a new tag in a tree. ggf create parent tags
        """
        pass

    def _xml_remove_tag_r():
        """
        rekursive routine to remove an xml tag and all its children
        """
        pass
    '''
    def get_fleur_modes(self):
        '''
        Retrieve information from the inp.xml file. 'Modes' are paths a FLEUR
        calculation can take, dependend on the input.
        i.e other files need to be copied before and after the calculation.
        common modes are: scf, jspin 2, dos, band, pot8, lda+U, eels, ...
        '''
        # TODO these should be retrieved by looking at the inpfile struture and
        # then setting the paths.
        # use methods from fleur parser...
        # For now they are hardcoded.
        #    'dos': '/fleurInput/output',
        #    'band': '/fleurInput/output',
        #    'pot8': '/fleurInput/calculationSetup/expertModes',
        #    'jspins': '/fleurInput/calculationSetup/magnetism',
        fleur_modes = {'jspins' : '', 'dos' : '', 'band' : '', 'pot8' : '', 'ldau' : ''}
        if 'inp.xml' in self.files:
            fleur_modes['jspins'] = self.inp_dict['calculationSetup']['magnetism']['jspins'] #['fleurInput']
            fleur_modes['dos'] = self.inp_dict['output']['dos']#'fleurInput']
            fleur_modes['band'] = self.inp_dict['output']['band']
            fleur_modes['pot8'] = self.inp_dict['calculationSetup']['expertModes']['pot8']

        return fleur_modes





    def _xml_set_first_attribv(self, xmltree, xpathn, attributename, attribv):
        """
        Routine sets the value of an attribute in the xml file

        :param: an etree a xpath from root to the attribute and the attribute value

        :return: None, or an etree

        Comment: Element.set will add the attribute if it does not exist,
                 xpath expression has to exist
        example: xml_set_first_attribv(tree, '/fleurInput/calculationSetup', 'band', 'T')
                 xml_set_first_attribv(tree, '/fleurInput/calculationSetup', 'dos', 'F')
        """

        root = xmltree.getroot()
        #print xpathn
        #print root.xpath(xpathn)
        #print type(attributename)
        #print type(attribv)
        if type(attribv) == type(''):
            root.xpath(xpathn)[0].set(attributename, attribv)
        else:
            root.xpath(xpathn)[0].set(attributename, str(attribv))
        #return xmltree
        #ToDO check if worked. else exception,

    def _xml_set_text(self, xmltree, xpathn, text):
        """
        Routine sets the text of a tag in the xml file
        Input: an etree a xpath from root to the tag and the text
        Output: none, an etree
        example:
        xml_set_text(tree, '/fleurInput/comment', 'Test Fleur calculation for AiiDA plug-in')
        but also cordinates and Bravais Matrix!:
        xml_set_text(tree, '/fleurInput/atomGroups/atomGroup/relPos', '1.20000 PI/3 5.1-MYCrazyCostant')
        """

        root = xmltree.getroot()
        root.xpath(xpathn)[0].text = text
        #return xmltree

    def get_structuredata(self):
        """
        This routine return an AiiDA Structure Data type produced from the inp.xml
        file.

        :return: StructureData node
        """
        from aiida.orm.data.structure import StructureData


        #Disclaimer: this routine needs some xpath expressions. these are hardcoded here,
        #therefore maintainance might be needed, if you want to circumvent this, you have
        #to get all the paths from somewhere.

        #######
        # all hardcoded xpaths used and attributes names:
        bravaismatrix_bulk_xpath = '/fleurInput/cell/bulkLattice/bravaisMatrix/'
        bravaismatrix_film_xpath = 'fleurInput/cell/filmLattice/bravaisMatrix/'
        species_xpath = '/fleurInput/atomSpecies/species'
        all_atom_groups_xpath = '/fleurInput/atomGroups/atomGroup'

        species_attrib_name = 'name'
        species_attrib_element = 'element'

        row1_tag_name = 'row-1'
        row2_tag_name = 'row-2'
        row3_tag_name = 'row-3'

        atom_group_attrib_species = 'species'
        atom_group_tag_abspos = 'absPos'
        atom_group_tag_relpos = 'relPos'
        atom_group_tag_filmpos = 'filmPos'
        ########

        if 'inp.xml' in self.files:
            # read in inpxml
            inpxmlfile = self.get_file_abs_path('inp.xml')#'./inp.xml'

            if self._schema_file_path: # Schema there, parse with schema
                xmlschema_doc = etree.parse(self._schema_file_path)
                xmlschema = etree.XMLSchema(xmlschema_doc)
                parser = etree.XMLParser(schema=xmlschema, attribute_defaults=True)
                tree = etree.parse(inpxmlfile, parser)
            else: #schema not there, parse without
                print 'parsing inp.xml without XMLSchema'
                tree = etree.parse(inpxmlfile)

            root = tree.getroot()

            # get cell matrix from inp.xml
            row1 = root.xpath(bravaismatrix_bulk_xpath + row1_tag_name)#[0].text.split()

            if row1: #bulk calculation
                print 'bulk'
                row1 = row1[0].text.split()
                row2 = root.xpath(bravaismatrix_bulk_xpath + row2_tag_name)[0].text.split()
                row3 = root.xpath(bravaismatrix_bulk_xpath + row3_tag_name)[0].text.split()
                # TODO? allow math?
                for i, cor in enumerate(row1):
                    row1[i] = float(cor)
                for i, cor in enumerate(row2):
                    row2[i] = float(cor)
                for i, cor in enumerate(row3):
                    row3[i] = float(cor)

                cell = [row1, row2, row3]
                # create new structure Node
                struc = StructureData(cell=cell)
                #set boundary conditions
                struc.pbc = [True, True, True]

            elif root.xpath(bravaismatrix_film_xpath + row1_tag_name): #film calculation
                print 'film'
                row1 = root.xpath(bravaismatrix_film_xpath + row1_tag_name)[0].text.split()
                row2 = root.xpath(bravaismatrix_film_xpath + row2_tag_name)[0].text.split()
                for i, cor in enumerate(row1):
                    row1[i] = float(cor)
                for i, cor in enumerate(row2):
                    row2[i] = float(cor)
                row3 = [0, 0, 0]#? TODO:what has it to be in this case?
                cell = [row1, row2, row3]
                # create new structure Node
                struc = StructureData(cell=cell)
                #set boundary conditions
                struc.pbc = [True, True, False]


            #get species for atom kinds
            species = root.xpath(species_xpath)

            species_name = root.xpath(species_xpath + '@' + species_attrib_name)
            species_element = root.xpath(species_xpath + '@' + species_attrib_element)
            # alternativ: loop over species and species.get(species_attrib_name)

            #save species info in a dict
            species_dict = {}
            for i, spec in enumerate(species_name):
                species_dict[spec] = {species_attrib_element: species_element[i]}

            # Now we have to get all atomgroups, look what their species is and
            # their positions are.
            # Then we append them to the structureData

            all_atom_groups = root.xpath(all_atom_groups_xpath)

            for atom_group in all_atom_groups:
                current_species = atom_group.get(atom_group_attrib_species)
                #print current_species
                group_atom_positions_abs = atom_group.xpath(atom_group_tag_abspos)
                group_atom_positions_rel = atom_group.xpath(atom_group_tag_relpos)
                group_atom_positions_film = atom_group.xpath(atom_group_tag_filmpos)
            #print group_atom_positions_abs, group_atom_positions_rel

            if group_atom_positions_abs: #we have absolut positions
                for atom in group_atom_positions_abs:
                    postion_a = atom.text.split()
                    # allow for math *, /
                    for i, pos in enumerate(postion_a):
                        if '/' in pos:
                            temppos = pos.split('/')
                            postion_a[i] = float(temppos[0])/float(temppos[1])
                        elif '*' in pos:
                            temppos = pos.split('*')
                            postion_a[i] = float(temppos[0])*float(temppos[1])
                        else:
                            postion_a[i] = float(pos)

                    # append atom to StructureData
                    struc.append_atom(
                        position=postion_a,
                        symbols=species_dict[current_species][species_attrib_element])

            elif group_atom_positions_rel: #we have relative positions
                # TODO: check if film or 1D calc, because this is not allowed! I guess
                for atom in group_atom_positions_rel:
                    postion_r = atom.text.split()
                    # allow for math * /
                    for i, pos in enumerate(postion_r):
                        if '/' in pos:
                            temppos = pos.split('/')
                            postion_r[i] = float(temppos[0])/float(temppos[1])
                        elif '*' in pos:
                            temppos = pos.split('*')
                            postion_r[i] = float(temppos[0])*float(temppos[1])
                        else:
                            postion_r[i] = float(pos)

                    # now transform to absolut Positions, BraivaisMatrix * relposVec
                    new_abs_pos = [postion_r[0]*row1[0] + postion_r[1]*row2[0]+ postion_r[2]*row3[0],
                                   postion_r[0]*row1[1] + postion_r[1]*row2[1]+ postion_r[2]*row3[1],
                                   postion_r[0]*row1[2] + postion_r[1]*row2[2]+ postion_r[2]*row3[2]]

                    # append atom to StructureData
                    struc.append_atom(
                        position=new_abs_pos,
                        symbols=species_dict[current_species][species_attrib_element])

            elif group_atom_positions_film: # Do we support mixture always, or only in film case?
                #either support or throw error
                #pass
                for atom in group_atom_positions_film:
                    # film pos are rel rel abs, therefore only transform first two coordinates
                    postion_f = atom.text.split()
                    # allow for math * /
                    for i, pos in enumerate(postion_f):
                        if '/' in pos:
                            temppos = pos.split('/')
                            postion_f[i] = float(temppos[0])/float(temppos[1])
                        elif '*' in postion_f[i]:
                            temppos = pos.split('*')
                            postion_f[i] = float(temppos[0])*float(temppos[1])
                        else:
                            postion_f[i] = float(pos)
                    # now transform to abspos BraivaisMatrix * relposVec
                    new_abs_pos = [postion_f[0]*row1[0] + postion_f[1]*row2[0],
                                   postion_f[0]*row1[1] + postion_f[1]*row2[1],
                                   postion_f[2]]

                    # append atom to StructureData
                    struc.append_atom(
                        position=new_abs_pos,
                        symbols=species_dict[current_species][species_attrib_element])
            else:
                print ('I should never get here, 1D not supported yet, '
                      'I only know relPos, absPos, filmPos')
                #TODO throw error
                #pass

            return struc
        else:
            print 'cannot get a StructureData because fleurinpdata has no inp.xml file yet'
            # TODO what to do in this case?
            return False





    def get_kpointsdata(self):
        """
        This routine returns an AiiDA kpoint Data type produced from the inp.xml
        file. This only works if the kpoints are listed in the in inpxml.

        :return: KpointsData node
        """
        from aiida.orm.data.array.kpoints import KpointsData


        #HINT, TODO:? in this routine, the 'cell' you might get in an other way
        #exp: StructureData.cell, but for this you have to make a structureData Node,
        # which might take more time for structures with lots of atoms.
        # then just parsing the cell from the inp.xml
        #as in the routine get_structureData

        #Disclaimer: this routine needs some xpath expressions.
        #these are hardcoded here, therefore maintainance might be needed,
        # if you want to circumvent this, you have
        #to get all the paths from somewhere.

        #######
        # all hardcoded xpaths used and attributes names:
        bravaismatrix_bulk_xpath = '/fleurInput/cell/bulkLattice/bravaisMatrix/'
        bravaismatrix_film_xpath = 'fleurInput/cell/filmLattice/bravaisMatrix/'
        kpointlist_xpath = '/fleurInput/calculationSetup/bzIntegration/kPointList/'

        kpoint_tag = 'kPoint'
        kpointlist_attrib_posscale = 'posScale'
        kpointlist_attrib_weightscale = 'weightScale'
        kpointlist_attrib_count = 'count'
        kpoint_attrib_weight = 'weight'
        row1_tag_name = 'row-1'
        row2_tag_name = 'row-2'
        row3_tag_name = 'row-3'
        ########

        if 'inp.xml' in self.files:
            # read in inpxml
            inpxmlfile = self.get_file_abs_path('inp.xml')

            if self._schema_file_path: # Schema there, parse with schema
                xmlschema_doc = etree.parse(self._schema_file_path)
                xmlschema = etree.XMLSchema(xmlschema_doc)
                parser = etree.XMLParser(schema=xmlschema, attribute_defaults=True)
                tree = etree.parse(inpxmlfile, parser)
            else: #schema not there, parse without
                print 'parsing inp.xml without XMLSchema'
                tree = etree.parse(inpxmlfile)

            root = tree.getroot()

            # get cell matrix from inp.xml

            row1 = root.xpath(bravaismatrix_bulk_xpath + row1_tag_name)#[0].text.split()

            if row1: #bulk calculation
                print 'bulk'
                row1 = row1[0].text.split()
                row2 = root.xpath(bravaismatrix_bulk_xpath + row2_tag_name)[0].text.split()
                row3 = root.xpath(bravaismatrix_bulk_xpath + row3_tag_name)[0].text.split()
                # TODO? allow math?
                for i, cor in enumerate(row1):
                    row1[i] = float(cor)
                for i, cor in enumerate(row2):
                    row2[i] = float(cor)
                for i, cor in enumerate(row3):
                    row3[i] = float(cor)

                cell = [row1, row2, row3]
                #set boundary conditions
                pbc1 = [True, True, True]

            elif root.xpath(bravaismatrix_film_xpath + row1_tag_name):
                #film calculation
                print 'film'
                row1 = root.xpath(bravaismatrix_film_xpath + row1_tag_name)[0].text.split()
                row2 = root.xpath(bravaismatrix_film_xpath + row2_tag_name)[0].text.split()
                for i, cor in enumerate(row1):
                    row1[i] = float(cor)
                for i, cor in enumerate(row2):
                    row2[i] = float(cor)
                row3 = [0, 0, 0]#? TODO:what has it to be in this case?
                cell = [row1, row2, row3]
                pbc1 = [True, True, False]

            # get kpoints
            kpoints = root.xpath(kpointlist_xpath + kpoint_tag)
            posscale = root.xpath(kpointlist_xpath + '@' + kpointlist_attrib_posscale)
            weightscale = root.xpath(kpointlist_xpath + '@' + kpointlist_attrib_weightscale)
            count = root.xpath(kpointlist_xpath + '@' + kpointlist_attrib_count)

            kpoints_pos = []
            kpoints_weight = []

            for kpoint in kpoints:
                kpoint_pos = kpoint.text.split()
                for i, kval in enumerate(kpoint_pos):
                    kpoint_pos[i] = float(kval)/float(posscale[0])
                    kpoint_weight = float(kpoint.get(kpoint_attrib_weight))/float(weightscale[0])
                    kpoints_pos.append(kpoint_pos)
                    kpoints_weight.append(kpoint_weight)

            kps = KpointsData(cell=cell)
            kps.pbc = pbc1

            kps.set_kpoints(kpoints_pos, cartesian=False, weights=kpoints_weight)
            return kps

        else:
            print 'cannot get a KpointsData because fleurinpdata has no inp.xml file yet'
            # TODO what to do in this case?
            return False






def inpxml_todict(parent, xmlstr):
    """
    Recursive operation which transforms an xml etree to
    python dictionaries and lists.
    Decision to add a list is if the tag name is in the given list tag_several

    :param parent: some xmltree, or xml element
    :param xmlstr: structure/layout of the xml file in xmlstr is tags_several:
                   a list of the tags, which should be converted to a list, not
                   a dictionary(because they are known to occur more often, and
                   want to be accessed in a list later.

    :return: a python dictionary
    """

    xmlstructure = xmlstr
    pos_switch_once1 = xmlstructure[0]
    pos_switch_several1 = xmlstructure[1]
    #pos_attrib_once1 = xmlstructure[2]
    int_attributes_once1 = xmlstructure[3]
    float_attributes_once1 = xmlstructure[4]
    string_attributes_once1 = xmlstructure[5]
    #pos_attrib_several1 = xmlstructure[6]
    int_attributes_several1 = xmlstructure[7]
    float_attributes_several1 = xmlstructure[8]
    string_attributes_several1 = xmlstructure[9]
    tags_several1 = xmlstructure[10]
    pos_text1 = xmlstructure[11]
    #pos_xpaths1 = xmlstructure[12]
    #expertkey1 = xmlstructure[13]

    return_dict = {}
    if parent.items():
        return_dict = dict(parent.items())
        # Now we have to convert lazy fortan style into pretty things for the Database
        for key in return_dict:
            if key in pos_switch_once1 or (key in pos_switch_several1):
                return_dict[key] = convert_from_fortran_bool(return_dict[key])

            elif key in int_attributes_once1 or (key in int_attributes_several1):
                # TODO int several
                return_dict[key] = int(return_dict[key])
            elif key in string_attributes_once1 or (key in string_attributes_several1):
                # TODO What attribute shall be set? all, one or several specific onces?
                return_dict[key] = str(return_dict[key])
            elif key in float_attributes_once1 or (key in float_attributes_several1):
                # TODO pressision?
                return_dict[key] = float(return_dict[key])
                #pass
            elif key in pos_text1:
                # TODO, prob not nessesary, since taken care of below check,
                pass
            else:
                pass
                # this key is not know to plug-in TODO maybe make this a method
                # of the parser and log this as warning, or add here make a log
                # list, to which you always append messages, pass them back to
                # the parser, who locks it then
                # raise TypeError("Parser wanted to convert the key:'{}' with
                # value '{}', from the inpxml file but the key is unknown to the
                # fleur plug-in".format(key, return_dict[key]))


    if parent.text: # TODO more detal, exp: relPos
        # has text, but we don't want all the '\n' s and empty stings in the database
        if parent.text.strip() != '': # might not be the best solution
            # set text
            return_dict = parent.text.strip()

    firstocc = True
    for element in parent:
        if element.tag in tags_several1:
            #make a list, otherwise the tag will be overwritten in the dict
            if firstocc: # is this the first occurence?
                #create a list
                return_dict[element.tag] = []
                return_dict[element.tag].append(inpxml_todict(element, xmlstructure))
                firstocc = False
            else: # occured before, a list already exists, therefore just add
                return_dict[element.tag].append(inpxml_todict(element, xmlstructure))
        else:
            #make dict
            return_dict[element.tag] = inpxml_todict(element, xmlstructure)

    return return_dict

def convert_from_fortran_bool(stringbool):
    """
    Converts a string in this case ('T', 'F', or 't', 'f') to True or False

    :param stringbool: a string ('t', 'f', 'F', 'T')

    :return: boolean  (either True or False)
    """
    true_items = ['True', 't', 'T']
    false_items = ['False', 'f', 'F']
    if isinstance(stringbool, str):
        if stringbool in false_items:
            return False
        if stringbool in true_items:
            return True
        else:
            raise InputValidationError(
            "A string: {} for a boolean was given, which is not 'True',"
            " 'False', 't', 'T', 'F' or 'f'".format(stringbool))
    else:
        raise TypeError("convert_to_fortran_bool accepts only a string or "
                     "bool as argument")


def convert_to_fortran_bool(boolean):
    """
    Converts a Boolean as string to the format definded in the input

    :param boolean: either a boolean or a string ('True', 'False', 'F', 'T')

    :return: a string (either 't' or 'f')
    """

    if isinstance(boolean, bool):
        if boolean:
            new_string = 'T'
            return new_string
        else:
            new_string = 'F'
            return new_string
    elif isinstance(boolean, str):
        if boolean == 'True' or boolean == 't' or boolean == 'T':
            new_string = 'T'
            return new_string
        elif boolean == 'False' or boolean == 'f' or boolean == 'F':
            new_string = 'F'
            return new_string
        else:
            raise InputValidationError(
                "A string: {} for a boolean was given, which is not 'True',"
                "'False', 't', 'T', 'F' or 'f'".format(boolean))
    else:
         raise TypeError("convert_to_fortran_bool accepts only a string or "
                         "bool as argument")

def convert_to_fortran_string(string):
    """
    converts some parameter strings to the format for the inpgen
    :param string: some string
    :returns: string in right format (extra "")
    """
    new_string = '"' + string + '"'
    return new_string
    '''
    if isinstance(string, str):
        new_string = '"' + string + '"'
        print new_string
        return new_string
        #if '"' in string:
        #    return new_string
        #else:
        #    new_string = '"' + string + '"'
        #    return new_string
    else:
        print (string)
        #return string
        raise TypeError("_convert_to_fortran_string accepts only a"
                        "string as argument, type {} given".format(type(string)))
    '''

def get_inpxml_file_structure():
    """
    This routine returns the structure/layout of the 'inp.xml' file.

    Basicly the plug-in should know from this routine, what things are allowed
    to be set and where, i.e all attributes and their xpaths.
    As a developer make sure to use this routine always of you need information
    about the inp.xml file structure.
    Therefore, this plug-in should be easy to adjust to other codes with xml 
    files as input files. Just rewrite this routine.

    For now the structure of the xmlinp file for fleur is hardcoded.
    If big changes are in the 'inp.xml' file, maintain this routine.
    TODO: Maybe this is better done, by reading the xml schema datei instead.
    And maybe it should also work without the schema file, do we want this?

    :param Nothing: TODO xml schema

    :return all_switches_once: list of all switches ('T' or 'F') which are allowed to be set
    :return all_switches_several: list of all switches ('T' or 'F') which are allowed to be set
    :return other_attributes_once: list of all attributes, which occur just once (can be tested)
    :return other_attributes_several: list of all attributes, which can occur more then once
    :return all_text: list of all text of tags, which can be set
    :return all_attrib_xpath: dictonary (attrib, xpath), of all possible attributes
                              with their xpath expression for the xmp inp
    :return expertkey: keyname (should not be in any other list), which can be
                       used to set anything in the file, by hand,
     (for experts, and that plug-in does not need to be directly maintained if
     xmlinp gets a new switch)
    """

    # All attributes (allowed to change?)

    #switches can be 'T' ot 'F' # TODO: alphabetical sorting
    all_switches_once = (
        'dos', 'band', 'secvar', 'ctail', 'frcor', 'l_noco',
        'ctail', 'swsp', 'lflip', 'off', 'spav', 'l_soc', 'soc66', 'pot8',
        'eig66', 'gamma', 'gauss', 'tria', 'invs', 'invs2', 'zrfs', 'vchk', 'cdinf',
        'disp', 'vacdos', 'integ', 'star', 'iplot', 'score', 'plplot', 'slice',
        'pallst', 'form66', 'eonly', 'bmt', 'relativisticCorrections', 'l_J', 'l_f')

    all_switches_several = ('calculate', 'flipSpin')

    int_attributes_once = ('numbands', 'itmax', 'maxIterBroyd', 'kcrel', 'jspins',
                           'gw', 'isec1', 'nx', 'ny', 'nz', 'ndir', 'layers',
                           'nstars', 'nstm', 'numkpt', 'nnne', 'lpr', 'count')
    float_attributes_once = ('Kmax', 'Gmax', 'GmaxXC', 'alpha', 'spinf', 'theta',
                             'phi', 'xa', 'thetad', 'epsdisp', 'epsforce',
                             'valenceElectrons', 'fermiSmearingEnergy', 'ellow',
                             'elup', 'scale', 'dTilda', 'dVac', 'minEnergy',
                             'maxEnergy', 'sigma', 'locx1', 'locy1', 'locx2',
                             'locy2', 'tworkf', 'minEigenval', 'maxEigenval')
    string_attributes_once = ('imix', 'mode', 'filename', 'latnam', 'spgrp',
                              'xcFunctional', 'fleurInputVersion', 'species')



    other_attributes_once = tuple(list(int_attributes_once) + list(float_attributes_once) + list(string_attributes_once))
    #print other_attributes_once
    other_attributes_once1 = (
        'isec1', 'Kmax', 'Gmax', 'GmaxXC', 'numbands', 'itmax', 'maxIterBroyd',
        'imix', 'alpha', 'spinf', 'kcrel', 'jspins', 'theta', 'phi', 'gw', 'lpr',
        'xa', 'thetad', 'epsdisp', 'epsforce', 'valenceElectrons', 'mode',
        'gauss', 'fermiSmearingEnergy', 'nx', 'ny', 'nz', 'ellow', 'elup',
        'filename', 'scale', 'dTilda', 'dVac', 'ndir', 'minEnergy', 'maxEnergy',
        'sigma', 'layers', 'nstars', 'locx1', 'locy1', 'locx2', 'locy2', 'nstm',
        'tworkf', 'numkpt', 'minEigenval', 'maxEigenval', 'nnne')


    int_attributes_several = ('atomicNumber', 'gridPoints', 'lmax', 'lnonsphr',
                              's', 'p', 'd', 'f', 'l', 'n', 'eDeriv', 'coreStates')
    float_attributes_several = ('value', 'magMom', 'radius', 'logIncrement')
    string_attributes_several = ('name', 'element', 'coreStates', 'type', 'relaxXYZ')
    other_attributes_several = (
        'name', 'value', 'element', 'atomicNumber', 'coreStates', 'magMom',
        'radius', 'gridPoints', 'logIncrement', 'lmax', 'lnonsphr', 's', 'p',
        'd', 'f', 'species', 'type', 'coreStates', 'l', 'n', 'eDeriv', 'relaxXYZ')

    # when parsing the xml file to a dict, these tags should become
    # list(sets, or tuples) instead of dictionaries.
    tags_several = ('atomGroup', 'relPos', 'absPos', 'filmPos', 'species')

    all_text = {'comment' : 1, 'relPos' : 3, 'filmPos' : 3, 'absPos' : 3,
                'row-1': 3, 'row-2':3, 'row-3' :3, 'a1': 1}
    #TODO all these (without comment) are floats, or float tuples.
    #Should be converted to this in the databas
    # changing the Bravais matrix should rather not be allowed I guess

    # all attribute xpaths

    #text xpaths(coordinates, bravaisMatrix)
    #all switches once, several, all attributes once, several
    all_attrib_xpath = {# text
        'comment': '/fleurInput/comment',
        'relPos': '/fleurInput/atomGroups/atomGroup/relPos',
        'filmPos': '/fleurInput/atomGroups/atomGroup/filmPos',
        'absPos': '/fleurInput/atomGroups/atomGroup/absPos',
        'row-1': '/fleurInput/cell/bulkLattice/bravaisMatrix',
        'row-2': '/fleurInput/cell/bulkLattice/bravaisMatrix',
        'row-3': '/fleurInput/cell/bulkLattice/bravaisMatrix',
        'a1': '/fleurInput/cell/filmLattice/a1', #switches once
        'dos': '/fleurInput/output',
        'band': '/fleurInput/output',
        'secvar': '/fleurInput/calculationSetup/expertModes',
        'ctail': '/fleurInput/calculationSetup/coreElectrons',
        'frcor': '/fleurInput/calculationSetup/coreElectrons',
        'l_noco': '/fleurInput/calculationSetup/magnetism',
        'l_J': '/fleurInput/calculationSetup/magnetism',
        'swsp': '/fleurInput/calculationSetup/magnetism',
        'lflip': '/fleurInput/calculationSetup/magnetism',
        'off': '/fleurInput/calculationSetup/soc',
        'spav': '/fleurInput/calculationSetup/soc',
        'l_soc': '/fleurInput/calculationSetup/soc',
        'soc66': '/fleurInput/calculationSetup/soc',
        'pot8': '/fleurInput/calculationSetup/expertModes',
        'eig66': '/fleurInput/calculationSetup/expertModes',
        'l_f': '/fleurInput/calculationSetup/geometryOptimization',
        'gamma': '/fleurInput/calculationSetup/bzIntegration/kPointMesh',
        'gauss': '',
        'tria': '',
        'invs': '',
        'zrfs': '',
        'vchk': '/fleurInput/output/checks',
        'cdinf': '/fleurInput/output/checks',
        'disp': '/fleurInput/output/checks',
        'vacdos': '/fleurInput/output',
        'integ': '/fleurInput/output/vacuumDOS',
        'star': '/fleurInput/output/vacuumDOS',
        'iplot': '/fleurInput/output/plotting',
        'score': '/fleurInput/output/plotting',
        'plplot': '/fleurInput/output/plotting',
        'slice': '/fleurInput/output',
        'pallst': '/fleurInput/output/chargeDensitySlicing',
        'form66': '/fleurInput/output/specialOutput',
        'eonly': '/fleurInput/output/specialOutput',
        'bmt': '/fleurInput/output/specialOutput',
        'relativisticCorrections': '/fleurInput/xcFunctional', #ALL_Switches_several
        'calculate': '/fleurInput/atomGroups/atomGroup/force',
        'flipSpin' : '/fleurInput/atomSpecies/species', #other_attributes_once
        'Kmax' : '/fleurInput/calculationSetup/cutoffs',
        'Gmax' : '/fleurInput/calculationSetup/cutoffs',
        'GmaxXC': '/fleurInput/calculationSetup/cutoffs',
        'numbands': '/fleurInput/calculationSetup/cutoffs',
        'itmax' : '/fleurInput/calculationSetup/scfLoop',
        'maxIterBroyd': '/fleurInput/calculationSetup/scfLoop',
        'imix': '/fleurInput/calculationSetup/scfLoop',
        'alpha': '/fleurInput/calculationSetup/scfLoop',
        'spinf': '/fleurInput/calculationSetup/scfLoop',
        'kcrel': '/fleurInput/calculationSetup/coreElectrons',
        'jspins': '/fleurInput/calculationSetup/magnetism',
        'theta': '/fleurInput/calculationSetup/soc',
        'phi' : '/fleurInput/calculationSetup/soc',
        'gw': '/fleurInput/calculationSetup/expertModes',
        'lpr': '/fleurInput/calculationSetup/expertModes',
        'isec1': '/fleurInput/calculationSetup/expertModes',
        'xa': '/fleurInput/calculationSetup/geometryOptimization',
        'thetad': '/fleurInput/calculationSetup/geometryOptimization',
        'epsdisp': '/fleurInput/calculationSetup/geometryOptimization',
        'epsforce': '/fleurInput/calculationSetup/geometryOptimization',
        'valenceElectrons':'/fleurInput/calculationSetup/bzIntegration',
        'mode': '/fleurInput/calculationSetup/bzIntegration',
        'fermiSmearingEnergy': '/fleurInput/calculationSetup/bzIntegration',
        'nx': '/fleurInput/calculationSetup/bzIntegration/kPointMesh',
        'ny': '/fleurInput/calculationSetup/bzIntegration/kPointMesh',
        'nz': '/fleurInput/calculationSetup/bzIntegration/kPointMesh',
        'count': '/fleurInput/calculationSetup/kPointCount',
        'ellow' : '/fleurInput/calculationSetup/energyParameterLimits',
        'elup': '/fleurInput/calculationSetup',
        'filename': '/fleurInput/cell/symmetryFile',
        'scale': '/fleurInput/cell/bulkLattice',
        'ndir': '/fleurInput/output/densityOfStates',
        'minEnergy': '/fleurInput/output/densityOfStates',
        'maxEnergy': '/fleurInput/output/densityOfStates',
        'sigma':' /fleurInput/output/densityOfStates',
        'layers': '/fleurInput/output/vacuumDOS',
        'nstars': '/fleurInput/output/vacuumDOS',
        'locx1': '/fleurInput/output/vacuumDOS',
        'locy1': '/fleurInput/output/vacuumDOS',
        'locx2': '/fleurInput/output/vacuumDOS',
        'locy2': '/fleurInput/output/vacuumDOS',
        'nstm': '/fleurInput/output/vacuumDOS',
        'tworkf': '/fleurInput/output/vacuumDOS',
        'numkpts': '/fleurInput/output/chargeDensitySlicing',
        'minEigenval': '/fleurInput/output/chargeDensitySlicing',
        'maxEigenval': '/fleurInput/output/chargeDensitySlicing',
        'nnne': '/fleurInput/output/chargeDensitySlicing',
        'dVac': '/fleurInput/cell/filmLattice',
        'dTilda': '/fleurInput/cell/filmLattice',
        'xcFunctional': '/fleurInput/xcFunctional/@name',#other_attributes_more
        'name': {'/fleurInput/constantDefinitions', '/fleurInput/xcFunctional',
                 '/fleurInput/atomSpecies/species'},
        'value': '/fleurInput/constantDefinitions',
        'element': '/fleurInput/atomSpecies/species',
        'atomicNumber': '/fleurInput/atomSpecies/species',
        'coreStates': '/fleurInput/atomSpecies/species',
        'magMom': '/fleurInput/atomSpecies/species',
        'radius': '/fleurInput/atomSpecies/species/mtSphere',
        'gridPoints': '/fleurInput/atomSpecies/species/mtSphere',
        'logIncrement': '/fleurInput/atomSpecies/species/mtSphere',
        'lmax': '/fleurInput/atomSpecies/species/atomicCutoffs',
        'lnonsphr': '/fleurInput/atomSpecies/species/atomicCutoffs',
        's': '/fleurInput/atomSpecies/species/energyParameters',
        'p': '/fleurInput/atomSpecies/species/energyParameters',
        'd': '/fleurInput/atomSpecies/species/energyParameters',
        'f': '/fleurInput/atomSpecies/species/energyParameters',
        'type': '/fleurInput/atomSpecies/species/lo',
        'l': '/fleurInput/atomSpecies/species/lo',
        'n': '/fleurInput/atomSpecies/species/lo',
        'eDeriv': '/fleurInput/atomSpecies/species/lo',
        'species': '/fleurInput/atomGroups/atomGroup',
        'relaxXYX': '/fleurInput/atomGroups/atomGroup/force'
    }
    #'constant': (name, value)
    #'xcFunctional' : name
    #'species' : (old_name, set_name)
    #'radius': (spcies_name, value), (species_id, value), (species_element, value)
    # all tags paths, make this a dict?

    all_tag_xpaths = (
        '/fleurInput/constantDefinitions',
        '/fleurInput/calculationSetup',
        '/fleurInput/calculationSetup/cutoffs',
        '/fleurInput/calculationSetup/scfLoop',
        '/fleurInput/calculationSetup/coreElectrons',
        '/fleurInput/calculationSetup/magnetism',
        '/fleurInput/calculationSetup/soc',
        '/fleurInput/calculationSetup/expertModes',
        '/fleurInput/calculationSetup/geometryOptimization',
        '/fleurInput/calculationSetup/bzIntegration',
        '/fleurInput/calculationSetup/kPointMesh',
        '/fleurInput/cell/symmetry',
        '/fleurInput/cell/bravaisMatrix',
        '/fleurInput/xcFunctional',
        '/fleurInput/xcFunctional/xcParams',
        '/fleurInput/atomSpecies/species',
        '/fleurInput/atomSpecies/species/mtSphere',
        '/fleurInput/atomSpecies/species/atomicCutoffs',
        '/fleurInput/atomSpecies/species/energyParameters',
        '/fleurInput/atomSpecies/species/coreConfig',
        '/fleurInput/atomSpecies/species/coreOccupation',
        '/fleurInput/atomGroups/atomGroup',
        '/fleurInput/atomGroups/atomGroup/relPos',
        '/fleurInput/atomGroups/atomGroup/absPos',
        '/fleurInput/atomGroups/atomGroup/filmPos',
        '/fleurInput/output/checks',
        '/fleurInput/output/densityOfStates',
        '/fleurInput/output/vacuumDOS',
        '/fleurInput/output/plotting',
        '/fleurInput/output/chargeDensitySlicing',
        '/fleurInput/output/specialOutput'
    )

    expertkey = 'other'
    returnlist = (all_switches_once,
                  all_switches_several,
                  other_attributes_once,
                  int_attributes_once,
                  float_attributes_once,
                  string_attributes_once,
                  other_attributes_several,
                  int_attributes_several,
                  float_attributes_several,
                  string_attributes_several,
                  tags_several,
                  all_text,
                  all_attrib_xpath,
                  expertkey)
    return returnlist


