# -*- coding: utf-8 -*-
from aiida.parsers.exceptions import OutputParsingError

__copyright__ = u"Copyright (c), 2016, Forschungszentrum Jülich GmbH, IAS-1/PGI-1, Germany. All rights reserved."
__license__ = "MIT license, see LICENSE.txt file"
__version__ = "0.27"
__contributors__ = "Jens Broeder"

#mainly created this Outputparsing error, that the user sees, that it comes from parsing a fleur calculation.
class FleurOutputParsingError(OutputParsingError):
    pass
    # if you want to do something special here do it