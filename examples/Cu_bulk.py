#!/usr/bin/env python
from aiida import load_dbenv, is_dbenv_loaded
if not is_dbenv_loaded():
    load_dbenv()
import sys,os 
from aiida.orm import Code, DataFactory, Computer, load_node
from aiida.workflows2.run import async, run
from aiida.orm.calculation.job.fleur_inp.fleurinputgen import FleurinputgenCalculation
from aiida.orm.calculation.job.fleur_inp.fleur import FleurCalculation

StructureData = DataFactory('structure')
ParameterData = DataFactory('parameter')
FleurinpData = DataFactory('fleurinp')

###############################
codename = 'fleur_inpgen_mac'
computer_name = 'local_mac'
codename2 = 'Fleur_mac'
###############################
bohr_a_0= 0.52917721092 # A

# first create a structure, or get it from somewhere
#s = load_node(601)
#parameters =load_node(602)

# Cu_delta_inp
a = 3.436364432671*bohr_a_0
cell = [[0.0,a,a],[a,0.,a],[a,a,0.]]
s = StructureData(cell=cell)
s.append_atom(position=(0.,0.,0.), symbols='Cu')

# A structure would be enough, then the input generator will choose default values, 
# if you want to set parameters you have to provide a ParameterData node with some namelists of the inpgen:
parameters = ParameterData(dict={
                          'title': 'Cu, fcc copper, bulk, delta project',
                          'atom':{'element' : 'Cu', 'rmt' : 2.28, 'jri' : 981, 'lmax' : 12, 'lnonsph' : 6},
                          'comp': {'kmax': 5.0, 'gmaxxc' : 12.5, 'gmax' : 15.0},
                          'kpt': {'div1' : 25, 'div2': 25, 'div3' : 25, 'tkb' : 0.0005}})


# now run the inputgenerator:
code = Code.get_from_string(codename)
computer = Computer.get(computer_name)
JobCalc = FleurinputgenCalculation.process()

attrs ={'max_wallclock_seconds' :180,
        'resources' : {"num_machines": 1},
        'withmpi' : False,
        'computer': computer
        }
inp = {'structure' : s, 'parameters' : parameters, 'code' : code}

f = run(JobCalc, _options=attrs, **inp)
fleurinp= f['fleurinpData']
fleurinpd = load_node(fleurinp.pk)

# now run a Fleur calculation ontop of an inputgen calculation
code = Code.get_from_string(codename2)
JobCalc = FleurCalculation.process()

attrs ={'max_wallclock_seconds' : 180, 'resources' : {"num_machines": 1} }
inp1 = {'code' : code, 'fleurinpdata' : fleurinpd}#'parent' : parent_calc,
f1 = run(JobCalc, _options=attrs, **inp1)

'''
# You can also run Fleur from a Fleur calculation and apply some changes to the input file.
# you should also specify the remote parent folder
#parentcalc = FleurCalculation.get_subclass_from_pk(parent_id)
fleurinp = f1['fleurinpData']
fleurinpd = load_node(fleurinp.pk).copy()
fleurinpd.set_changes({'dos' : T})

inp2 = {'code' : code, 'fleurinpdata' : fleurinpd}#'parent' : parent_calc,
f2 = run(JobCalc, _options=attrs, **inp2)
'''
